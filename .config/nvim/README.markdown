# This is my NeoVim config.

note: neovim uses python 3 installed in ~.config/nvim/env

Makesure you clone the repository to ~/git/dotfiles/vim/

and

ln -s ~/git/dotfiles/vim ~/.config/nvim

On this link you will find the readme on how to install the plugins:
https://github.com/Optixal/neovim-init.vim

Use install.sh or do the installation manually:

```sh
sudo add-apt-repository ppa:neovim-ppa/stable

# Install nvim (and its dependencies: pip3, git), Python 3 and ctags (for tagbar)
sudo apt install curl neovim python3 python3-pip exuberant-ctags -y

# Install virtualenv to containerize dependencies
pip3 install --user virtualenv
python3 -m virtualenv ~/.config/nvim/env

# Install pip modules for Neovim within the virtual environment created
source ~/.config/nvim/env/bin/activate
pip install neovim jedi psutil setproctitle
deactivate

#for haskell ctags
stack install hasktags haskdogs

# Install vim-plug plugin manager
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# (Optional but recommended) Install a nerd font for icons and a beautiful airline bar (https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts) (I'll be using Roboto Mono for Powerline)
curl -fLo ~/.local/share/fonts/Roboto\ Mono\ Nerd\ Font\ Complete.ttf --create-dirs https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/RobotoMono/Medium/complete/Roboto%20Mono%20Medium%20Nerd%20Font%20Complete.ttf 

# (Optional) Alias vim -> nvim
echo "alias vim='nvim'" >> ~/.bash_aliases

# Enter Neovim and install plugins, ignore warnings shown (missing colorschemes, functions, etc.), simply keep press ENTER
nvim
:PlugInstall
```

## Features

#### Using leader

* `,`            - Map leader, nearly all my custom mappings starts with pressing the comma key
* `,q`           - Sidebar filetree viewer (NERDTree)
* `,w`           - Sidebar classes, functions, variables list (TagBar)
* `\`            - Toggle both NERDTree and TagBar
* `,ee`          - Change colorscheme (with fzf fuzzy finder)
* `,ea`          - Change Airline theme
* `,e1`          - Color mode: Dracula (Dark)
* `,e2`          - Color mode: Seoul256 (Between Dark & Light)
* `,e3`          - Color mode: Forgotten (Light)
* `,e4`          - Color mode: Zazen (Black & White)
* `,r`           - Refresh/source ~/.config/nvim/init.vim
* `,t`           - Trim all trailing whitespaces
* `,a`           - Auto align variables (vim-easy-align), eg. do `,a=` while your cursor is on a bunch of variables to align their equal signs
* `,s`           - New terminal in horizontal split
* `,vs`          - New terminal in vertical split
* `,d`           - Automatically generate Python docstrings while cursor is hovering above a function or class
* `,f`           - Fuzzy find a file (fzf)
* `,g`           - Toggle Goyo mode (Goyo), super clean and minimalistic viewing mode
* `,rh`           - Toggle rainbow parentheses highlighting
* `,j`           - Set filetype to "journal" which makes the syntax highlighting beautiful when working on regular text files and markdown
* `,k`           - Toggle coloring of hex colors
* `,n`           - Hackernews vertical windows (requires internet) (vim-hackernews)
* `,c<Space>`    - Toggle comment for current line (Nerd Commenter)
* `,w/W/b/B/e/E` - CamelCase motions
* `,b`           - List all possible buffers and accept a new buffer argument [1]
* `,z`           - Toggles the syntastic quickfix and location list

#### Language Server
* `<F5>`          - Context Menu
* `,lh`           - Hover
* `,ld`           - Defintion
* `,lr`           - Rename
* `,lf`           - Formatting
* `,lb`           - References
* `,la`           - Code Action
* `,ls`           - Document Symbol

#### Haskell
* `,i`           - Will ignore all current warnings in hlint (haskell)
* `,ct`          - Generate ctags for the current filetype.
* `,gi`          - insert type signature
* `,gc`          - Generate all cases
* `,gt`          - Generate type signature
* `.ge`          - eraze type signature

#### Tabs

* `tn`          - :newtab
* `tl`          - :tabnext
* `th`          - :tabprev

#### Non-leader

* `<Alt-r>`       - Toggle RGB color picker
* `<Tab>`         - Next buffer
* `<Shift-Tab>`   - Previous buffer
* `<Caps>`        - Capslock mapped to esc. (might not work on all machines)
* `¤`             - Mapped to $
* `<Alt>j/k`      - To move the current line up or down
* `<Ctrl>h/j/k/l` - To move between splits
* `<Space>`       - To repeat a macro
* `<F6>`          - Toggle spellchecker (default british english)
* `<F7>`          - Swedish spellchecker
* `<F3>`          - autoformat
