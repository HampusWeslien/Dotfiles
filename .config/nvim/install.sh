sudo add-apt-repository ppa:neovim-ppa/stable

# Install nvim (and its dependencies: pip3, git), Python 3 and ctags (for tagbar)
sudo apt install curl neovim python3 python3-pip exuberant-ctags -y

# Install virtualenv to containerize dependencies
pip3 install --user virtualenv
python3 -m virtualenv ~/.config/nvim/env

# Install pip modules for Neovim within the virtual environment created
source ~/.config/nvim/env/bin/activate
pip install neovim jedi psutil setproctitle
deactivate

# Install vim-plug plugin manager
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# (Optional but recommended) Install a nerd font for icons and a beautiful airline bar (https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts) (I'll be using Roboto Mono for Powerline)
curl -fLo ~/.local/share/fonts/Roboto\ Mono\ Nerd\ Font\ Complete.ttf --create-dirs https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/RobotoMono/Medium/complete/Roboto%20Mono%20Medium%20Nerd%20Font%20Complete.ttf 

# (Optional) Alias vim -> nvim
echo "alias vim='nvim'" >> ~/.bash_aliases

# Enter Neovim and install plugins, ignore warnings shown (missing colorschemes, functions, etc.), simply keep press ENTER
nvim
:PlugInstall

